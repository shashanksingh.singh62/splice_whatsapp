import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:splice_whatsapp/http_service.dart';
import 'package:splice_whatsapp/model/single_user_reponse.dart';
import 'package:splice_whatsapp/model/user.dart';

class SingleUserScreen extends StatefulWidget {
  @override
  _SingleUserScreenState createState() => _SingleUserScreenState();
}

class _SingleUserScreenState extends State<SingleUserScreen> {
  HttpService http;

  SingleUserResponse singleUserResponse;
  User user;

  bool isLoading = false;

  Future getUser() async {
    Response response;
    try {
      isLoading = true;

      response = await http.getRequest("/data/api/user/1pRsh5nXDIH3pjEOZ17A");

      isLoading = false;

      if (response.statusCode == 200) {
        setState(() {
          singleUserResponse = SingleUserResponse.fromJson(response.data);
          user = singleUserResponse.user;
        });
      } else {
        print("There is some problem status code not 200");
      }
    } on Exception catch (e) {
      isLoading = false;
    //  print(e);
      print("exceeeeeeee");
    }
  }

  @override
  void initState() {

    http = HttpService();

    getUser();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ge Single user"),
      ),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : user != null ? Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(user.picture),
            Container(height: 16,),
            Text("Hello, ${user.firstName} ${user.lastName}")
          ],
        ),
      ): Center(child : Text("No User Object")),
    );
  }
}