import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splice_whatsapp/home.dart';
import 'package:splice_whatsapp/screen/list_user_screen.dart';
import 'package:splice_whatsapp/screen/single_user_screen.dart';

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp]);

  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Color(0xff075e54),
          primaryColorLight: Color(0xff08d261)
      ),
      home: HomeScreen(),

    );

  }
}
