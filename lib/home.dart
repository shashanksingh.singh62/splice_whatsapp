import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splice_whatsapp/calls.dart';
import 'package:splice_whatsapp/http_service.dart';
import 'package:splice_whatsapp/model/list_user_response.dart';
import 'package:splice_whatsapp/model/user.dart';
import 'package:splice_whatsapp/screen/chat_page.dart';
import 'package:splice_whatsapp/search/search.dart';
import 'package:splice_whatsapp/status.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  bool isLoading = false;

  HttpService http;

  ListUserResponse listUserResponse;

  List<User> users;

  Future getListUser() async {
    Response response;
    try {
      isLoading = true;

      response = await http.getRequest("data/api/user");

      isLoading = false;

      if (response.statusCode == 200) {
        setState(() {
          listUserResponse = ListUserResponse.fromJson(response.data);
          users = listUserResponse.users;
        });
      } else {
        print("There is some problem status code not 200");
      }
    } on Exception catch (e) {
      isLoading = false;
      print(e);
    }
  }

//  @override
//  void initState() {
////    http = HttpService();
////
////    getListUser();
//
//    super.initState();
//  }


  @override
  void initState() {
    http = HttpService();

    getListUser();

    _tabController = new TabController(length: 4, initialIndex: 1, vsync: this)

      ..addListener(() {
       setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('WhatsApp'),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: () {

          }),
          IconButton(icon: Icon(Icons.more_vert), onPressed: () {})
        ],
        bottom: TabBar(
          isScrollable: true,
          controller: _tabController,
          indicatorColor: Colors.white,
          labelPadding: EdgeInsets.all(12),
          tabs: [
            Icon(Icons.camera_alt),
            Container(
                width: 90, alignment: Alignment.center, child: Text('CHATS')),
            Container(
                width: 90, alignment: Alignment.center, child: Text('STATUS')),
            Container(
                width: 90, alignment: Alignment.center, child: Text('CALLS'))
          ],
        ),
      ),
      body:

      TabBarView(
        controller: _tabController,
        children: [
          Text('CAMERA'),

          isLoading
              ? Center(child: CircularProgressIndicator())
              : users != null
              ? ListView.builder(
            itemBuilder: (context, index) {
              final user = users[index];

              return ListTile(
                title: Text(user.firstName),
                leading: CircleAvatar(backgroundImage: NetworkImage(user.picture),),
                subtitle: Text(user.id),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ChatPage()),);
                  },
              );
            },
            itemCount: users.length,
          )
              : Center(
            child: Text("No User Object"),
          ),


          //  START STATUS
          ListView.builder(
              itemCount: 1,
              itemBuilder: (context, index) {
                return Column(
                  children: [

                    StatusScreen(
                      images: 'user/tata.jpg',

                    ),

                  ],
                );
              }),

          // START CALLS
          ListView.builder(
              itemCount: 5,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    CallsScreen(
                      images: 'user/tata.jpg',
                      title: 'call',
                    ),
                    CallsScreen(
                      images: 'user/tata.jpg',
                      title: 'call',
                    ),

                  ],
                );
              }),



        ],
      ),
      floatingActionButton: _tabController.index == 1  ? FloatingActionButton(
        onPressed: (){},
        backgroundColor: Theme.of(context).primaryColorLight,
        child:Icon(Icons.message

        ) ,):FloatingActionButton(
        onPressed: (){},
        backgroundColor: Theme.of(context).primaryColorLight,
        child:Icon(Icons.camera_alt

        ) ,) ,
    );
  }
}