import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  @JsonKey(name : "id")
  String id;

  @JsonKey(name : "email")
  String email;

  @JsonKey(name : "firstName")
  String firstName;

  @JsonKey(name : "lastName")
  String lastName;

  @JsonKey(name : "picture")
  String picture;

  User();

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
