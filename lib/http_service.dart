import 'package:dio/dio.dart';



class HttpService{
  Dio _dio;

  final baseUrl = "https://dummyapi.io/";

  HttpService(){
    _dio = Dio(BaseOptions(
      baseUrl: baseUrl,
      headers: {
      "app-id": "60992da1ea03b472bb9a096b"
      },
    ),
    );

    initializeInterceptors();
  }


  Future<Response> getRequest(String endPoint) async{
    Response response;

    try {
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      print(e.message);
      throw Exception(e.message);
    }

    return response;

  }
  Future <Response> postRequest(String endPoint,dynamic data) async{
    Response response;

    try{
      response = await _dio.post(endPoint, data: data);
    }
    on DioError catch (e) {
      print(e.message);
      throw Exception(e.message);
    }
    return response;
  }
  static dynamic requestInterceptor(RequestOptions options) async {
    // Get your JWT token
    const token = '';
    options.headers.addAll({"Authorization": "Bearer: $token"});
    return options;
  }

  initializeInterceptors(){
    _dio.interceptors.add(InterceptorsWrapper(
        onError: (error){
          print(error.message);
        },
        onRequest: (request){
          print("${request.method} ${request.path}");
        },
        onResponse: (response){
          print(response.data);
        }
    ));
  }
}